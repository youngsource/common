<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use Rakit\Validation\Rule;
use Rakit\Validation\Validation;
use function in_array;

/**
 * Class EmptyIfRule
 * @package Laudis\Calculators\Rules
 */
final class EmptyIfRule extends Rule
{
    /** @var string */
    private $otherValueKey;

    /**
     * EmptyIfRule constructor.
     * @param string $otherValueKey
     */
    public function __construct(string $otherValueKey)
    {
        $this->otherValueKey = $otherValueKey;
        $this->fillableParams = [$this->otherValueKey];
        $this->setMessage(
            ':attribute mag enkel worden ingevuld indien ' . $this->otherValueKey . ' niet gevraagd wordt'
        );
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;
        $otherValue = $validation->getValue($this->otherValueKey);
        if ($otherValue !== null && $otherValue !== false) {
            return in_array($value, [null, '', 0], true);
        }

        return true;
    }
}
