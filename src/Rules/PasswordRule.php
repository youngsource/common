<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\User;
use Laudis\UserManagement\UserManager;
use PDO;
use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class PasswordRule
 * @package Laudis\Calculators\Rules
 */
final class PasswordRule extends Rule
{
    /**
     * @var PasswordHasherInterface
     */
    private $hasher;
    /**
     * @var PDO
     */
    private $pdo;
    /**
     * @var UserManager
     */
    private $manager;

    /**
     * PasswordRule constructor.
     * @param PasswordHasherInterface $hasher
     */
    public function __construct(PasswordHasherInterface $hasher, PDO $pdo, UserManager $manager)
    {
        $this->hasher = $hasher;
        $this->pdo = $pdo;
        $this->setMessage('Het wachtwoord is niet correct.');
        $this->manager = $manager;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;

        // Only validatate if the validation is passing, otherwise this might give away too much info to the client.
        if ($validation->passes()) {
            $email = $validation->getValue('email');
            if ($email === null) {
                /** @var User $user */
                $user = $this->manager->getLoggedInUser();
                $email = $user->getEmail();
            }

            $hash = $this->pdo->query("SELECT `password_hash` FROM users WHERE email = '$email'")->fetchColumn();
            return $this->hasher->check($value, $hash);
        }
        return true;
    }
}
