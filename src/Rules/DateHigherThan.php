<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use DateTime;
use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class DateHigherThan
 * @package Laudis\Calculators\Rules
 */
final class DateHigherThan extends Rule
{
    /**
     * @var string
     */
    private $otherDateKey;
    /**
     * @var string
     */
    private $otherDateFormat;
    /**
     * @var string
     */
    private $thisDateFormat;

    /**
     * DateHigherThan constructor.
     * @param string $otherDateKey
     * @param string $thisDateFormat
     * @param string $otherDateFormat
     */
    public function __construct(
        string $otherDateKey,
        string $thisDateFormat = 'Y-m-d',
        string $otherDateFormat = 'Y-m-d'
    ) {
        $this->otherDateKey = $otherDateKey;
        $this->otherDateFormat = $otherDateFormat;
        $this->thisDateFormat = $thisDateFormat;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;
        if ($value === null) {
            return true;
        }
        $otherDate = DateTime::createFromFormat($this->otherDateFormat, $validation->getValue($this->otherDateKey));
        $thisDate = DateTime::createFromFormat($this->thisDateFormat, $value);

        return $thisDate->getTimestamp() >= $otherDate->getTimestamp();
    }
}
