<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use InvalidArgumentException;
use Rakit\Validation\Rule;
use Rakit\Validation\Validation;
use function array_key_exists;
use function array_keys;

/**
 * Class RequireIfValueIs
 * @package Laudis\Calculators\Rules
 */
final class IfValueIsRule extends Rule
{
    /** @var string */
    private $column;
    /** @var mixed */
    private $value;
    /** @var null */
    private $operand;

    /**
     * RequireIfValueIs constructor.
     * @param string $column
     * @param mixed $value
     * @param null $operand
     */
    public function __construct(string $column, $value, $operand = null)
    {
        if (!array_key_exists($operand, $this->mappings())) {
            throw new InvalidArgumentException($this->errorMessage());
        }
        $this->column = $column;
        $this->value = $value;
        $this->operand = $operand;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;
        if ($this->mappings()[$this->operand]($validation->getValue($this->column), $this->value)) {
            return $value !== null;
        }
        return $value === null;
    }

    /**
     * @return array
     */
    private function mappings(): array
    {
        return [
            '<' => static function ($lhs, $rhs) {
                return $lhs < $rhs;
            },
            '<=' => static function ($lhs, $rhs) {
                return $lhs <= $rhs;
            },
            '===' => static function ($lhs, $rhs) {
                return $lhs === $rhs;
            },
            '>' => static function ($lhs, $rhs) {
                return $lhs > $rhs;
            },
            '>=' => static function ($lhs, $rhs) {
                return $lhs === $rhs;
            }
        ];
    }

    /**
     * @return string
     */
    private function errorMessage(): string
    {
        return 'the operand must be one of: ' . implode(', ', array_keys($this->mappings()));
    }
}
