<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use PDO;
use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class ContextualInsertionRule
 * @package Laudis\Calculators\Rules
 */
final class ContextualInsertionRule extends Rule
{
    /**
     * @var PDO
     */
    private $pdo;
    /**
     * @var string
     */
    private $table;
    /**
     * @var string
     */
    private $contextualIdColumn;
    /**
     * @var string
     */
    private $dateColumn;
    /**
     * @var string
     */
    private $dateValueKey;
    /**
     * @var string
     */
    private $idKey;
    /**
     * @var string
     */
    private $idColumn;

    /**
     * ContextualInsertionRule constructor.
     * @param PDO $pdo
     * @param string $table
     * @param string $contextualIdColumn
     * @param string $dateColumn
     */
    public function __construct(
        PDO $pdo,
        string $table,
        string $idKey = 'id',
        string $idColumn = 'id',
        string $contextualIdColumn = 'contextual_id',
        string $dateColumn = 'added_since',
        string $dateValueKey = 'addedSince'
    ) {
        $this->pdo = $pdo;
        $this->table = $table;
        $this->contextualIdColumn = $contextualIdColumn;
        $this->dateColumn = $dateColumn;
        $this->dateValueKey = $dateValueKey;
        $this->idKey = $idKey;
        $this->idColumn = $idColumn;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        $table = $this->table;
        $idColumn = $this->contextualIdColumn;
        $dateColumn = $this->dateColumn;
        /** @var Validation $validation */
        $validation = $this->validation;
        $date = $validation->getValue($this->dateValueKey);

        $result = $this->pdo->query("
            SELECT * FROM $table
            WHERE $idColumn = '$value' AND $dateColumn = '$date'
        ")->fetch(PDO::FETCH_ASSOC);

        if ($result !== false) {
            // If a result is found, we need to make sure it is not the same value that is just being updated.
            return $validation->getValue($this->idKey) !== null &&
                ((string) $validation->getValue($this->idKey)) === ((string) $result[$this->idColumn]);
        }
        return true;
    }
}
