<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use Rakit\Validation\Rule;
use function is_int;
use function is_numeric;
use const PHP_FLOAT_MAX;
use const PHP_FLOAT_MIN;
use const PHP_INT_MAX;
use const PHP_INT_MIN;

/**
 * Class NumberBetweenRule
 * @package Laudis\Calculators\Rules
 */
final class NumberBetweenRule extends Rule
{
    /** @var float|int */
    private $min;
    /** @var float|int */
    private $max;

    /**
     * NumberBetweenRule constructor.
     * @param int|float $min
     * @param int|float $max
     */
    public function __construct($min = null, $max = null)
    {
        $this->min = $min;
        $this->max = $max;
        $this->decideMessage();
    }

    /**
     * Decides the message derived from the min and max values.
     */
    private function decideMessage(): void
    {
        if ($this->min === PHP_INT_MIN || $this->min === PHP_FLOAT_MIN) {
            if ((float)$this->max === 0.0) {
                $this->setMessage('dit getal moet negatief zijn');
            } else {
                $this->setMessage('dit getal moet minder zijn dan: ' . $this->max);
            }
        } elseif ($this->max === PHP_INT_MAX || $this->max === PHP_FLOAT_MAX) {
            if ((float)$this->min === 0.0) {
                $this->setMessage('dit getal moet positief zijn');
            } else {
                $this->setMessage('dit getal moet meer zijn dan: ' . $this->min);
            }
        } else {
            $this->setMessage('dit getal moet tussen: ' . $this->min . ' en ' . $this->max . ' liggen');
        }
    }

    /**
     * Makes a number rule with the rules min and max.
     * If the value is null, it will be PHP_INT_{BOUND} or PHP_FLOAT_{BOUND} if the other value
     * is either an int or float.
     *
     * @param int|float|null $min
     * @param int|float|null $max
     * @return NumberBetweenRule
     */
    public static function make($min = null, $max = null): NumberBetweenRule
    {
        if ($min === null) {
            $min = is_int($max) ? PHP_INT_MIN : PHP_FLOAT_MIN;
        }
        if ($max === null) {
            $max = is_int($min) ? PHP_INT_MAX : PHP_FLOAT_MAX;
        }
        return new NumberBetweenRule($min, $max);
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if (!is_numeric($value)) {
            $this->setMessage('Dit moet een geheel getal zijn.');
            return false;
        }
        if (is_int($this->min)) {
            $value = (int)$value;
        } else {
            $value = (float)$value;
        }
        return ($value >= $this->min && $value <= $this->max);
    }
}
