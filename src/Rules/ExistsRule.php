<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use PDO;
use Rakit\Validation\Rule;

/**
 * Class ExistsRule
 * @package Laudis\Calculators\Rules
 */
final class ExistsRule extends Rule
{
    /**
     * @var PDO
     */
    private $pdo;
    /**
     * @var string
     */
    private $table;
    /**
     * @var string
     */
    private $column;

    /**
     * ExistsRule constructor.
     * @param PDO $pdo
     * @param string $table
     * @param string $column
     */
    public function __construct(PDO $pdo, string $table, string $column, string $message)
    {
        $this->pdo = $pdo;
        $this->table = $table;
        $this->column = $column;
        $this->setMessage($message);
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        $table = $this->table;
        $column = $this->column;

        $result = $this->pdo->query("SELECT * FROM $table WHERE $column = \"$value\"")->fetch(PDO::FETCH_ASSOC);
        return $result !== false;
    }
}
