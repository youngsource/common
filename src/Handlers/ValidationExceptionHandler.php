<?php
/** @noinspection PhpUndefinedClassInspection */
declare(strict_types=1);

namespace Laudis\Common\Handlers;

use InvalidArgumentException;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Exceptions\ValidationException;
use Laudis\Common\Presenters\ValidationPresenter;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class ValidationExceptionHandler
 * Handles all validation exceptions.
 *
 * @package Laudis\Calculators\Handlers
 */
final class ValidationExceptionHandler extends BaseHandler
{
    /** @var \Laudis\Common\Presenters\ValidationPresenter */
    private $presenter;

    /**
     * ValidationExceptionHandler constructor.
     * @param ResponseWriterInterface $jsonToResponse
     * @param \Laudis\Common\Presenters\ValidationPresenter $presenter
     */
    public function __construct(ResponseWriterInterface $jsonToResponse, ValidationPresenter $presenter)
    {
        parent::__construct($jsonToResponse);
        $this->presenter = $presenter;
    }

    /**
     * Handles the validation exception.
     *
     * @param ResponseInterface $response
     * @param Throwable $exception
     * @return ResponseInterface|null
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function handleException(ResponseInterface $response, Throwable $exception): ?ResponseInterface
    {
        if ($exception instanceof ValidationException) {
            $body = [
                'message' => $exception->getMessage()
            ];
            $validation = $exception->getValidation();
            if ($validation !== null) {
                $body['data'] = $this->presenter->present($validation);
            }

            return $this->writeJson($response, $body, 422);
        }
        return null;
    }
}
