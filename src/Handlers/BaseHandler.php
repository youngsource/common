<?php
declare(strict_types=1);

namespace Laudis\Common\Handlers;

use Laudis\Common\Contracts\ExceptionHandlerInterface;
use Laudis\Common\Contracts\ResponseWriterInterface as ResponseWriter;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class BaseHandler
 * Implements the basic behaviour of an exception handler.
 *
 * @package Laudis\Calculators\Handlers
 */
abstract class BaseHandler implements ExceptionHandlerInterface
{
    /** @var ResponseWriter */
    private $jsonToResponse;

    /**
     * BaseHandler constructor.
     * @param ResponseWriter $jsonToResponse
     */
    public function __construct(ResponseWriter $jsonToResponse)
    {
        $this->jsonToResponse = $jsonToResponse;
    }

    /**
     * Writes the data to the given response and overrides the status code if provided.
     * @param Response $response
     * @param array $data
     * @param int|null $statusCode
     * @return Response
     */
    final protected function writeJson(Response $response, array $data, int $statusCode = null): Response
    {
        return $this->jsonToResponse->write($response, $data, $statusCode);
    }
}
