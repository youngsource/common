<?php
declare(strict_types=1);

namespace Laudis\Common\Contracts;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Interface ActionController
 * @package Laudis\Calculators\Contracts
 */
interface ActionController
{
    /**
     * Invokes the action of the controller.
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function __invoke(Request $request, Response $response): Response;
}
