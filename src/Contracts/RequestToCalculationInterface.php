<?php
declare(strict_types=1);

namespace Laudis\Common\Contracts;

use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Exceptions\CalculatorNotFoundException;
use Laudis\Common\Exceptions\ValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface RequestToCalculationInterface
 *
 * @package Laudis\Calculators\Contracts
 */
interface RequestToCalculationInterface
{
    /**
     * Runs the strategy of transforming the server request to the calculation result.
     *
     * @param ServerRequestInterface $request
     * @param string $calculator
     * @return CalculationResultInterface
     *
     * @throws \Laudis\Common\Exceptions\ValidationException  Thrown if the validation fails
     * @throws CalculatorNotFoundException Thrown if the calculator is not found.
     */
    public function run(ServerRequestInterface $request, string $calculator): CalculationResultInterface;
}
