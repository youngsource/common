<?php
declare(strict_types=1);

namespace Laudis\Common\Contracts;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Interface PreFlightCorsControllerInterface
 * @package Laudis\Calculators\Contracts
 */
interface PreFlightCorsControllerInterface
{
    /**
     * Allow all option requests in the case of a pre flight.
     *
     * @param Request $request
     * @param Response $response
     *
     * @return Response
     */
    public function allowOptions(Request $request, Response $response): Response;

    /**
     * Falls back to catch all other routes so they don't trigger a 405 status.
     *
     * @param Request $request
     * @param Response $response
     *
     * @return Response
     */
    public function fallback(Request $request, Response $response): Response;
}
