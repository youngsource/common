<?php
declare(strict_types=1);

namespace Laudis\Common\Calculators;

use Laudis\Common\Exceptions\ValidationException;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\Validation;
use function is_array;

/**
 * Trait ValidatesRequestTrait
 * @package Laudis\Calculators
 */
trait ValidatesRequest
{
    /**
     * @return Validation
     */
    abstract protected function getValidation(): Validation;

    /**
     * @param ServerRequestInterface $request
     */
    protected function validateRequest(ServerRequestInterface $request): array
    {
        $body = $request->getParsedBody();
        if (!is_array($body)) {
            throw new ValidationException(null, 'The body could not be parsed into an array.');
        }
        $this->getValidation()->validate($body);
        if ($this->getValidation()->fails()) {
            throw new ValidationException($this->getValidation());
        }
        return $body;
    }
}
