<?php
declare(strict_types=1);

namespace Laudis\Common\Auxiliaries;

use Laudis\Common\Contracts\RouterInterface;
use Psr\Http\Message\UriInterface;
use Slim\App;
use Slim\Interfaces\RouteGroupInterface;
use Slim\Interfaces\RouteInterface;

/**
 * Class Router
 * @package Laudis\Calculators\Auxiliaries
 */
final class Router implements RouterInterface
{
    /** @var App */
    private $app;

    /**
     * Router constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * Add GET route
     *
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function get(string $pattern, $callable): RouteInterface
    {
        return $this->app->get($pattern, $callable);
    }

    /**
     * Add POST route
     *
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function post(string $pattern, $callable): RouteInterface
    {
        return $this->app->post($pattern, $callable);
    }

    /**
     * Add PUT route
     *
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function put(string $pattern, $callable): RouteInterface
    {
        return $this->app->put($pattern, $callable);
    }

    /**
     * Add PATCH route
     *
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function patch(string $pattern, $callable): RouteInterface
    {
        return $this->app->patch($pattern, $callable);
    }

    /**
     * Add DELETE route
     *
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function delete(string $pattern, $callable): RouteInterface
    {
        return $this->app->delete($pattern, $callable);
    }

    /**
     * Add OPTIONS route
     *
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function options(string $pattern, $callable): RouteInterface
    {
        return $this->app->delete($pattern, $callable);
    }

    /**
     * Add route for any HTTP method
     *
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function any(string $pattern, $callable): RouteInterface
    {
        return $this->app->any($pattern, $callable);
    }

    /**
     * Add route with multiple methods
     *
     * @param  string[] $methods Numeric array of HTTP method names
     * @param  string $pattern The route URI pattern
     * @param  callable|string $callable The route callback routine
     *
     * @return RouteInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function map(array $methods, string $pattern, $callable): RouteInterface
    {
        return $this->app->map($methods, $pattern, $callable);
    }

    /**
     * Add a route that sends an HTTP redirect
     *
     * @param string $from
     * @param string|UriInterface $towards
     * @param int $status
     *
     * @return RouteInterface
     */
    public function redirect(string $from, $towards, int $status = 302): RouteInterface
    {
        return $this->app->redirect($from, $towards, $status);
    }

    /**
     * Route Groups
     *
     * This method accepts a route pattern and a callback. All route
     * declarations in the callback will be prepended by the group(s)
     * that it is in.
     *
     * @param string $pattern
     * @param callable $callable
     *
     * @return RouteGroupInterface
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function group(string $pattern, callable $callable): RouteGroupInterface
    {
        return $this->app->group($pattern, $callable);
    }
}
