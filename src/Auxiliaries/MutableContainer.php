<?php
declare(strict_types=1);

namespace Laudis\Common\Auxiliaries;

use Laudis\Common\Contracts\MutableContainerInterface;
use Slim\Container;

/**
 * Class MutableContainer
 * @package Laudis\Calculators\Auxiliaries
 */
final class MutableContainer extends Container implements MutableContainerInterface
{

    /**
     * @param string $identifier
     * @return bool
     */
    public function has($identifier): bool
    {
        return parent::has($identifier);
    }

    /**
     * @param string $identifier
     * @param $value
     */
    public function set(string $identifier, $value): void
    {
        $this[$identifier] = $value;
    }

}
