<?php
declare(strict_types=1);

namespace Laudis\Common\Auxiliaries;

use Exception;
use Laudis\Common\Contracts\AppInterface;
use Laudis\Common\Contracts\MutableContainerInterface;
use Laudis\Common\Contracts\RouterInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App as SlimApplication;
use function settings;

/**
 * Class Application
 *
 * @package Laudis\Calculators\Auxiliaries
 */
final class Application implements AppInterface
{
    /** @var SlimApplication */
    private $app;
    /** @var MutableContainer */
    private $mutableContainer;
    /** @var \Laudis\Common\Contracts\RouterInterface */
    private $router;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->mutableContainer = new MutableContainer(['settings' => settings()]);
        $this->app = new SlimApplication($this->mutableContainer);
        $this->router = new Router($this->app);
    }

    /**
     * Enable access to the DI container by consumers of $app
     *
     * @return \Laudis\Common\Contracts\MutableContainerInterface
     */
    public function getContainer(): MutableContainerInterface
    {
        return $this->mutableContainer;
    }

    /**
     * Add middleware
     *
     * This method prepends new middleware to the app's middleware stack.
     *
     * @param  callable|string $callable The callback routine
     *
     * @return Application
     */
    public function add($callable): Application
    {
        $this->app->add($callable);

        return $this;
    }

    /**
     * @return \Laudis\Common\Contracts\RouterInterface
     */
    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    /**
     * Run application
     *
     * This method traverses the application middleware stack and then sends the
     * resultant Response object to the HTTP client.
     *
     * @param bool|false $silent
     * @return ResponseInterface
     *
     * @throws Exception
     */
    public function run($silent = false): ResponseInterface
    {
        return $this->app->run($silent);
    }

    /**
     * Process a request
     *
     * This method traverses the application middleware stack and then returns the
     * resultant Response object.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     *
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $this->app->process($request, $response);
    }

    /**
     * Send the response to the client
     *
     * @param ResponseInterface $response
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function respond(ResponseInterface $response): void
    {
        $this->app->respond($response);
    }

    /**
     * Invoke application
     *
     * This method implements the middleware interface. It receives
     * Request and Response objects, and it returns a Response object
     * after compiling the routes registered in the Router and dispatching
     * the Request object to the appropriate Route callback routine.
     *
     * @param  ServerRequestInterface $request The most recent Request object
     * @param  ResponseInterface $response The most recent Response object
     *
     * @return ResponseInterface
     * @throws Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->app->__invoke($request, $response);
    }
}
